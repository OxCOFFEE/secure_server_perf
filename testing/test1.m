pkg load statistics;

hold on;
avgs = [0 0; 0 0];
x = [];
y1 = []; # time taken
y2 = []; # num_clients
boxes = [];
for file = 1:32
  data = csvread(sprintf("test1_nat_single_%d.csv", file), 1, 0);
  times = data(:,1)'; # Transpose via prime'
  y=data(:,2)';
  x = [x times];
  y1 = [y1 data(:,2)'];
  y2 = [y2 data(:,3)'];
  boxes = [boxes y']; # Save new column on boxes matrix
endfor

x -= min(x); # Move to 0 (absolute timestamps start on Unix epoch)
y1 = 1./y1; # Convert time to throughput
y2 += 1; # HACK cont_multi dataset started num_clients on 0

hold on;
scatter(x,y1,'r');
[ax, p1, p2]=plotyy([], [], x,y2);
hold off;

xlabel("Timestamp [s]");
ylabel(ax(1), "Throughput [transfers/sec]");
ylabel(ax(2), "Number of (concurrent) clients");
ylim(ax(1), [0 14]);
ylim(ax(2), [0 16]);
title("Concurrent throughput (1MB transfer) (Native)");
legend({"Throughput";"Num. clients"});
grid on;
