pkg load statistics;

hold on;
avgs = [0 0; 0 0];
boxes = [];
filenames = {"32", "1kb", "32kb", "1mb", "32mb"};
for file = 1:5
  data = csvread(sprintf("test3_nat_single_%s.csv", filenames{file}), 1, 0);
  y=data(:,2)';
  boxes = [boxes y']; # Save new column on boxes matrix
endfor

hold on;
stats = boxplot(boxes);
set(gca(),'yscale','log');

coeffs = polyfit(1:5, log2(stats(3,:))/5, 1);
coeffs
function y = evalloglog(coeffs, x)
  y = 32^(coeffs(1)*x + coeffs(2));
endfunction
plot(1:5, arrayfun(@(x) evalloglog(coeffs, x), 1:5), "*-;Best fit;");
hold off;

xlabel("Requested file size");
ylabel("Latency [s]");
title("Single-transfer latency (variable size transfer) (Native, single thread)");
xlim([0.5 5.5]);
ylim([0.003 40]);
xticklabels({"32 bytes", "1KiB", "32KiB", "1MiB", "32 MiB"});
legend("location", "southeast");
grid on;
