# secure_server_perf

Código para medir el desempeño de [secure_server](https://bitbucket.org/OxCOFFEE/secure_server). 

Puede usarse con el código nativo, y también incluye los archivos de configuración necesarios para
replicar la aplicación en contenedores de Docker, junto con un contenedor de NGINX como balanceador
de carga/proxy reverso.

## Instalación

1. Clonar [secure_server](https://bitbucket.org/OxCOFFEE/secure_server) a `secure_server`, compilar 
el servidor y el cliente con `make`. 
1. Clonar este repositorio a `secure_server_perf`.
1. Crear carpetas `nginx` y `files` dentro de la carpeta del repositorio (`secure_server_perf`).
1. Copiar `secure_server/file_server` a `secure_server_perf/files`.
1. Copiar `secure_server/file_client` a `secure_server_perf/testing`.

## Uso

1. Para iniciar el servidor replicado en Docker, ejecutar `secure_server_perf/run.sh`. Esto crea el
archivo de configuración de Docker (`secure_server_perf/docker-compose.yml`) y de NGINX
(`secure_server_perf/nginx/nginx.conf`).
1. Para probar el servidor nativo, no ejecutar el servidor en Docker, sino `./secure_server_perf/files/file_server -c 80`
1. Asegurarse de que el servidor esté disponible en `localhost:8000`.
1. Ejecutar `secure_server_perf/testing/test1.py` o `secure_server_perf/testing/test3.py`.
1. El script creará una serie de archivos CSV.
1. Ejecutar `secure_server_perf/testing/test1.m`, `test2.m` o `test3.m` para crear las gráficas.
Editar el nombre de los archivos en la llamada a `csvread` para que coincida con los CSVs creados.

## Resultados

### Prueba 1
Mide el throughput de la aplicación conforme varía el número de clientes simultáneos.
El código actual varía entre 1 y 16 clientes, con 8 instancias del servidor.

La imagen de abajo muestra un gráfico generado en esta prueba.

![plot test 1](report/images/test1_cont_multi.png)

### Prueba 2
Mide la latencia de una única transferencia conforme varía el número de clientes simultáneos.
El código actual varía entre 1 y 16 clientes, con 8 instancias del servidor.

Usa los mismos datos de la prueba 1, por eso no es necesario un script `secure_server_perf/testing/test2.py`.

La imagen de abajo muestra un gráfico generado en esta prueba.

![plot test 2](report/images/test2_cont_multi.png)

### Prueba 3
Mide la latencia de una única transferencia de tamaño variable.
El código actual varía entre 1 y 16 clientes, con 8 instancias del servidor y 12 clientes simultáneos.

La imagen de abajo muestra un gráfico generado en esta prueba.

![plot test 3](report/images/test3_cont_multi.png)

