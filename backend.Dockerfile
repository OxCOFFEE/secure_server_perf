FROM alpine:3.12.0 AS builder

RUN apk update && \
    apk add build-base git make
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.31-r0/glibc-2.31-r0.apk
RUN apk add glibc-2.31-r0.apk

RUN git clone https://bitbucket.org/OxCOFFEE/secure_server
WORKDIR /secure_server
#RUN git checkout singlethreaded
RUN make libraries
RUN make server

WORKDIR /secure_server/filedir
COPY entrypoint.sh /secure_server
RUN ["chmod", "+x", "../entrypoint.sh"]
ENTRYPOINT ["/secure_server/entrypoint.sh"]
