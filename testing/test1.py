#!/usr/bin/env python3

import subprocess
import timeit
import multiprocessing as mp
import queue
import time
import abc

NUM_WORKERS = 16
NUM_TESTS = 100

class Job:
    def __init__(self, command):
        self.command = command
        
class BaseJobResult(metaclass=abc.ABCMeta):   
    @staticmethod
    def get_header():
        """
        Returns the CSV header for a result file of the type.
        
        Implement on child classes
        """
        return
        
    @abc.abstractmethod
    def get_line(self):
        """
        Returns a line for a CSV file, that represents the current job result.
        
        Implement on child classes
        """
        return
        
class TimeJobResult(BaseJobResult):
    def __init__(self, timestamp, time):
        self.timestamp = timestamp
        self.time = time

    @staticmethod
    def get_header():
        return "Timestamp,ElapsedTime,CurrWorkers"
        
    def get_line(self, current_workers):
        return "{},{},{}".format(self.timestamp, self.time, current_workers)
        
def get_average(q):
    total = 0
    num_elems = 0
    while not q.empty():
        total += q.get().time
        num_elems += 1
    return total / num_elems

def worker_main(command_queue, report_queue):
    while True:
        try:
            job = command_queue.get(block=True, timeout=0.1)
            # process job, measure time, save on result
            result = timeit.timeit("subprocess.run(['../program/file_client', 'localhost', '80'], text=True, input='{}', stdout=subprocess.PIPE)".format(job.command),
                                   setup="import subprocess", number=1)
            report_queue.put_nowait(TimeJobResult(time.time(), result))
        except queue.Empty:
            print("Bye!")
            return # Queue was empty, we're done

n=0
for i in list(range(NUM_WORKERS))+list(range(NUM_WORKERS))[::-1]:
    print("NUM_WORKERS: ", i)
    n += 1
    command_q = mp.Queue()
    report_q = mp.Queue()

    for _ in range(NUM_TESTS):
        command_q.put(Job(r"GET 1mb.txt\nBYE\n")) # Add jobs to pool
        
    pool = mp.Pool(i+1, worker_main, (command_q, report_q)) # Spawn NUM_WORKERS workers to process jobs
        
    pool.close() # No more jobs will be submitted to pool!
    pool.join() # Waits for all workers to return

    # report_q contains all execution times
    with open("{}.csv".format(n), "w") as f:
        f.write("{}\n".format(TimeJobResult.get_header()))
        while not report_q.empty():
            f.write("{}\n".format(report_q.get().get_line(i+1)))
