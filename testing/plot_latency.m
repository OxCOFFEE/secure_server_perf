function retval = plot_latency(filename)
  data = csvread(filename, 1, 0);
  x=data(:,1)'; # Transpose via prime'
  x -= min(x); # Move to 0 (absolute timestamps start on Unix epoch)
  y=data(:,2)';
  scatter(x,y);
  
  retval = [ max(x) sum(y) / length(y) ];
endfunction