pkg load statistics;

hold on;
avgs = [0 0; 0 0];
boxes = [];
for file = 1:16
  data = [csvread(sprintf("test1_nat_single_%d.csv", file), 1, 0) 
          csvread(sprintf("test1_nat_single_%d.csv", 33-file), 1, 0) ];
  y=data(:,2)';
  boxes = [boxes y']; # Save new column on boxes matrix
endfor

hold on;
boxplot(boxes);
hold off;

xlabel("Number of concurrent clients");
ylabel("Latency [s]");
title("Single-transfer latency (1MB transfer) (Native, single thread)");
xlim([0.5 16.5]);
ylim([0 1.6]);
xticks(1:16);

#legend({"Latency";"Num. clients"});
grid on;
